![alt text](https://gitlab.com/vlzdavid12/spinning_center_gym/-/raw/main/ScreenShot.png)

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Demo
[Demo Page](https://spinningcenter.vercel.app/)

## Contributing
[Emotion](https://emotion.sh/docs/introduction)
[React Gsap](https://bitworking.github.io/react-gsap/)
[Styled Component](https://styled-components.com/)





