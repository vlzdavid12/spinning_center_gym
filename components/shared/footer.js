import React from 'react';
import {css} from '@emotion/css';
import Image from "next/image";
import Link from "next/link";
import playStore from "../../public/assets/google-play.png";
import appStore from "../../public/assets/app-store.png";

const Footer = () => {
    return (
        <>
            <div id="trigger_footer"/>
            <div className={css` color: #fff;
              text-align: center;
              background-color: #000507;
              padding-top: 10px;
              padding-bottom: 20px;
              margin-bottom: 20px;`}>
                <p>Descarga Gratis App</p>
                <Link href="https://play.google.com/store/apps/details?id=com.proyecto.spinngcentergym.tgcustom&hl=es_CO0"><a><Image src={playStore} width={135} height={60} alt="playstore" /></a></Link>
                <Link href="https://apps.apple.com/co/app/spinning-center-gym/id1071043372"><a><Image src={appStore} width={135} height={60} alt="appstore" /></a></Link>
            </div>
        </>
    )
}
export default Footer
