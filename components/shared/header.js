import React from 'react';
import {css} from "@emotion/css";
import {Tween} from 'react-gsap';
import styled from '@emotion/styled';
import Image from "next/image";
import Link from "next/link";
import logoImage_2 from '../../public/assets/logo2.jpg'
import logoImage from '../../public/assets/logo.png';
import bannerVector from '../../public/assets/banner.svg';
import bannerImage from '../../public/assets/image-banner.png';

let Description = styled.div`
  position: absolute;
  top: 6em;
  right: 8em;
  color: #fff;
  z-index: 999;
  font-family: 'Singo-1Goqv';
  @media (max-width: 780px) {
    width: 100%;
    top: 2em;
    right: -26em;
  }
  @media (max-width: 480px) {
    right: -2em;
  }`

let Sidebar = styled.div`
  background-color: #ffbd00;
  position: relative;
  margin-top: -30px;
  @media (max-width: 790px) {
    background-color: #0c1317;
    height: 50vh;
  }
  @media (max-width: 380px) {
    height: 30vh;
  }
`

const Header = () => {
    return (
        <>
            <Tween to={{y: '-20px'}} duration={2} ease="back.out(1.7)">
                <div className={css`
                  background-color: #0c1317;
                  height: 15vh;
                  display: flex;
                  justify-content: center;
                  align-items: center;
                  padding-top: 20px;
                  position: relative;
                  z-index: 999;
                  @media (max-width: 790px) {
                    padding: 40px;
                    margin-top: 20px;
                  }
                  @media (max-width: 480px) {
                    padding: 0px;
                    margin-top: 5px;
                  }
                `}>
                    <Link href="https://www.spinningcentergym.com/"><a className={css`hover: {
                      cursor: pointer;
                    }`}><Image src={logoImage} height={60} width={155} alt="spinning_center"/></a></Link>
                </div>
            </Tween>

            <Sidebar>
                <div className={css`
                  position: absolute;
                  top: 50%;
                  left: 50%;
                  transform: translate(-50%, -50%);
                  @media (max-width: 790px) {
                    display: none;
                  }
                `}>
                    <Image src={logoImage_2} width={250} height={80} alt="gym_spinner_center"/>
                </div>
                <Tween from={{x: '2000px'}} duration={3}>
                    <div className={css` width: 100%;
                      background-repeat: no-repeat;
                      background-size: cover;
                      position: relative;
                      z-index: 999;
                      top: -1px;
                      height: 450px;
                      background-image: url(${bannerVector.src});
                      @media (max-width: 790px) {
                        height: 50vh;
                      }
                      @media (max-width: 480px) {
                        height: 30vh;
                      }

                    `}/>
                </Tween>

                <Tween from={{x: '-2000px'}} duration={3}>
                    <div className={css`
                      background-color: black;
                      width: 70%;
                      height: 450px;
                      position: absolute;
                      top: -1px;
                      left: 0px;
                      z-index: 99;
                      background-image: url(${bannerImage.src});
                      clip-path: polygon(0 0, 100% 0%, 60% 100%, 0% 100%);
                      @media (max-width: 790px) {
                        top: -60px;
                        width: 70%;
                        height: 240px;
                        padding: 0px;
                        margin: 0px;
                        z-index: 99;
                      }
                      @media (max-width: 480px) {
                        display: none;
                      }
                    `}/>
                </Tween>

                <Tween from={{x: '3000px'}} duration={3}>
                    <Description>
                        <h4 className={css`
                          font-size: 9vh;
                          margin: 0;
                          @media (max-width: 780px) {
                            font-size: 14vh;
                          }
                          @media (max-width: 480px) {
                            font-size: 7vh;
                          }
                        `}>Que tus grandes</h4>
                        <h3 className={css`
                          font-size: 7vh;
                          margin: 0;
                          color: #ffbd00;
                          @media (max-width: 780px) {
                            font-size: 12vh;
                          }
                          @media (max-width: 480px) {
                            font-size: 6vh;
                          }
                        `}>Retos Continuen.</h3>
                    </Description>
                </Tween>
            </Sidebar>
        </>
    )
}
export default Header;
