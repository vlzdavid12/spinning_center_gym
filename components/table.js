import React from 'react';
import styled from "@emotion/styled";
import {css} from "@emotion/css";
import Image from "next/image";
import {gsap} from "gsap";
import {Tween} from 'react-gsap';
import {ScrollTrigger} from 'gsap/dist/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

import ImageDoctor from '../public/assets/doctor.png';
import ImageCheck from '../public/assets/check.png';
import ImageSab from '../public/assets/sap.png';

const TableList = styled.div`
  background-color: #000507;
  padding: 10px;
  color: #fff;
  width: 33%;
  margin: 0.5rem;
  padding-bottom: 35px;
  border-radius: 8px;
  position: relative;
  @media (max-width: 790px) {
    width: 90%;
    margin: auto;
    margin-bottom: 50px;
  }
`;

const Price = styled.h4`
  color: #ffbd00;
  text-align: center;
  font-size: 5vh;
  margin: auto;
  font-family: 'Singo-1Goqv';
  @media (max-width: 790px) {
    font-size: 12vh;
  }
  @media (max-width: 380px) {
    font-size: 5vh;
  }
`;


const Button = styled.button`
  background-color: #ffbd00;
  color: #fff;
  text-align: center;
  padding: 10px 20px;
  border-radius: 20px;
  border: 2px solid #f8a818;
  font-size: 18px;
  display: flex;
  justify-content: center;
  margin: 20px auto 0px auto;
  &:hover {
    background-color: #f8a818;
    cursor: pointer;
  }

  @media (max-width: 790px) {
    position: relative;
    margin: auto;
    display: block;
  }
`;

const imageIcon = (icon) => {
    switch (icon) {
        case'check':
            return ImageCheck.src;
        case 'sad':
            return ImageSab.src;
        case'doctors':
            return ImageDoctor.src;
        default:
            return null;
    }
};

const Table = ({list, animateNumber, animateDirection}) => {

    function directionAnimate() {
        switch (animateDirection) {
            case 'left':
                return {
                    x: animateNumber,
                }
            case 'right':
                return {
                    x: animateNumber,
                }
            case 'center':
                return {
                    y: animateNumber,
                }
            default:
                return null;
        }
    }

    if (!list) return null;
    const {iconTable: {src}, title, price, plan} = list[0];
    let option = directionAnimate();

    return (
        <>
            <div id="trigger_table"/>
            <Tween
                from={{
                    ...option,
                    scrollTrigger: {
                        trigger: '#trigger_table',
                        start: '-200 center',
                        end: "+=500", // end after scrolling 500px beyond the start
                        scrub: 1,
                        markers: false,
                    },
                }}
            >

                <TableList>
                    <div className={css`
                      display: flex;
                      justify-content: center;
                      align-items: center;
                    `}><Image src={src} width={100} height={100} alt={title}/></div>

                    <h2 className={css`
                      text-align: center;
                    `}>{title.toUpperCase()}</h2>
                    <ul className={css`list-style: none;`}>
                        {list.map((item, i) => {
                            if (item.msg) {
                                return <li key={i} className={css`font-size: 12px;
                                  padding: 0.5rem;
                                  display: flex;
                                  align-items: center;
                                  margin-left: -28px;

                                  :before {
                                    background-image: url(${imageIcon(item.icon)});
                                    background-repeat: no-repeat;
                                    background-size: contain;
                                    padding: 20px;
                                    width: auto;
                                    height: auto;
                                    margin-right: 10px;
                                    position: relative;
                                    display: block;
                                    content: '';
                                  }`}>{item.msg}</li>
                            }
                        })}
                    </ul>
                    <Price>{`$ ${price} /${plan}`}</Price>
                    <Button>Comprar</Button>
                </TableList>
            </Tween>
        </>
    )
}
export default Table;
