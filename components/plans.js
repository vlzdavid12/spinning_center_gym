import React from 'react';
import {css} from '@emotion/css'
import styled from '@emotion/styled'
import Link from "next/link";
import Table from "./table";
import {gsap} from "gsap";
import {Tween} from 'react-gsap';
import {ScrollTrigger} from 'gsap/dist/ScrollTrigger';
import IconTable_1 from '../public/assets/icon-table-1.png';
import IconTable_2 from '../public/assets/icon-table-2.png';
import IconTable_3 from '../public/assets/icon-table-3.png';

gsap.registerPlugin(ScrollTrigger);
const Title = styled.h2`
    font-size: 6vh;
    color: #ffbd00;
    font-family: 'Singo-1Goqv';
    text-align: center;
    margin: 20px 0px;
    @media(max-width: 790px){
        font-size: 13vh;
}
  @media(max-width: 380px){
    font-size: 5vh;
  }
`;

const Paragraph = styled.p`
  color: #0c1317;
  text-align: center;
  width: 900px;
  margin: auto;
  padding: 20px;
  font-size: 14px;
  line-height: 24px;
  @media (max-width: 790px) {
    width: 100%;
  }`;

const List_1 = [
    {title: 'solo app', iconTable: IconTable_1, price: '29.000', plan: 'mes'},
    {icon: 'check', msg: 'Acceso ilimitado a nuestra APP Oficial de \n Spinning Center Gym'},
    {icon: 'check', msg: 'Diseño de tu propia rutina'},
    {icon: 'check', msg: 'Video Tutoriales explicativos'},
    {icon: 'check', msg: 'Más de 80 clases virtuales'},
    {icon: 'check', msg: 'Cardio, Fuerza, Cycling y mucho más'},
    {icon: 'sad', msg: 'No incluye asesoría personalizada'},
    {icon: 'sad', msg: 'No incluye asesoría con entrenador o \n consulta con fisioterapeuta'},
]


const List_2 = [
    {title: 'plan virtual', iconTable: IconTable_2, price: '69.000', plan: 'mes'},
    {icon: 'check', msg: 'Acceso ilimitado a nuestra APP Oficial de \n Spinning Center Gym'},
    {icon: 'check', msg: 'Diseño de tu propia rutina'},
    {icon: 'check', msg: 'Video Tutoriales explicativos'},
    {icon: 'check', msg: 'Más de 80 clases virtuales'},
    {icon: 'check', msg: 'Cardio, Fuerza, Cycling y mucho más'},
    {
        icon: 'check',
        msg: 'Asesoría con entrenador o consulta con \n fisioterapeuta. Se debe elegir un único tipo \n de consulta ya sea con fisioterapeuta o \n entrenador.'
    },
    {
        icon: 'doctors',
        msg: '*Incluye una única consulta al mes,\n la cual se realiza por medio de videollamada,\n meet o zoom.'
    },
]

const List_3 = [
    {title: 'plan virtual pluss', iconTable: IconTable_3, price: '89.000', plan: 'mes'},
    {icon: 'check', msg: 'Acceso ilimitado a nuestra APP Oficial de \n Spinning Center Gym'},
    {icon: 'check', msg: 'Diseño de tu propia rutina'},
    {icon: 'check', msg: 'Video Tutoriales explicativos'},
    {icon: 'check', msg: 'Más de 80 clases virtuales'},
    {icon: 'check', msg: 'Cardio, Fuerza, Cycling y mucho más'},
    {icon: 'check', msg: 'Consultas ilimitadas'},
    {
        icon: 'doctors',
        msg: '*Incluye una única consulta al mes,\n la cual se realiza por medio de videollamada,\n meet o zoom.'
    },

]

const Plans = () => {
    return (
        <>

            <div className={css`background-color: #fff;
              padding-top: 20px;`}>
                <Tween from={{y: '100px', opacity: 0}} duration={3} ease="back.out(1.7)">
                    <Title>PLANES VIRTUALES</Title>
                    <div className={css`
                      padding-bottom: 50px;
                    `}>
                        <Paragraph>Adquiere nuestros planes del 2021 en todas nuestros gimnasios en Colombia. Pereira,
                            Ibagué,
                            Neiva, Cali (sede oeste),
                            Barranquilla (sedes Barcelona Plaza y Plaza del parque), Cartagena, Madrid, Cajicá y Bogotá
                            (Sedes
                            de
                            Unicentro de Occidente,
                            Rosales, Suba, Calle 102, Calle 140, Calle 122, Cedritos y Parque 93) y retoma tu vida con
                            toda
                            la
                        </Paragraph>
                        <p className={css`color: #ffbd00;
                          font-size: 20px;
                          text-align: center`}>#ActitudSpinning.</p>
                    </div>
                </Tween>
            </div>

            <div className={css`
              background-color: #0c1317;
              position: relative;
              padding: 1px;
              display: block;`}>
                <div className={css`
                  display: flex;
                  max-width: 1000px;
                  margin: auto;
                  top: -40px;
                  position: relative;
                  @media (max-width: 790px) {
                    display: block;
                  }
                `}>
                    <Table list={List_1} animateNumber="-600" animateDirection="left"/>
                    <Table list={List_2} animateNumber="200" animateDirection="center"/>
                    <Table list={List_3} animateNumber="600" animateDirection="right"/>
                </div>
            </div>
            <div id="trigger_description" />
            <div className={css`
              background-color: #0c1317;
              position: relative;
              display: block;
            `}>
                <div
                    className={css`margin: auto;
                      text-align: center;
                      max-width: 950px;
                      font-size: 14px;
                      padding: 10vh 0;
                      @media(max-width: 790px){
                           padding: 20px;
                      }
                    `}>


                        <p className={css`text-align: center;
                          color: #fff`}>
                            <span className={css`color: #ffbd00`}>TÉRMINOS Y CONDICIONES FREE PASS 3 DÍAS:</span> La
                            cortesía de los 3
                            días es válida solo para usuarios nuevos que nunca antes han redimido una cortesía en
                            Spinning
                            Center Gym o usuarios con más de 6 meses de inactividad en nuestras sedes y debe solicitarse
                            en
                            el siguiente <br/> link: <Link href="https://bit.ly/3eowMXv"><a
                            className={css`color: #ffbd00;

                              :hover {
                                color: #f8a818
                              }`}>https://bit.ly/3eowMXv</a></Link> Los 3 días gratis
                            deben redimirse de forma continua y
                            en una sola sede.</p>
                        <p className={css`text-align: center;
                          color: #fff`}>Se debe presentar el documento de identidad en el gimnasio,
                            el código que genera la aplicación Coronapp y reservar el tiempo de entrenamiento a través
                            de
                            nuestra App para poder ingresar, además de cumplir con nuestros protocolos de bioseguridad,
                            conócelos aquí: <Link href="https://www.spinningcentergym.com/protocolos">
                                <a className={css`color: #ffbd00;

                                  :hover {
                                    color: #f8a818
                                  }`}>www.spinningcentergym.com/protocolos</a></Link>. Fecha límite de
                            redención de la cortesía
                            31 de marzo del 2021. </p>
                        <p className={css`text-align: center;
                          color: #fff`}>No se permite el ingreso de menores de 14 años. Nos reservamos el derecho de
                            admisión. </p>

                </div>
            </div>
        </>
    )
}
export default Plans
